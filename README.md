# Solar Panel Box and LED Array

Project goal was to create a cheap DIY portable station with USB ports powered by solar panels for use in a remote environment. Example applications included running a mini-fridge and multiple LED strips at night for a Burning Man campsite. 

![alt text](solar_box.jpg "Finished Box")

Arduino-based code was designed to run relay/fan to prevent electronics from overheating in extreme desert environment. Used with multiple solar panels, charge controller, and inverter. Teensy 3.1 cycled through simple LED effects using generic WS2811 LED strips connected in parallel. 

![alt text](solar_sch_final.PNG "Solar Box Wiring")
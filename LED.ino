#define USE_OCTOWS2811
#include <OctoWS2811.h>
#include <FastLED.h>

//#define DATA_PIN 6
//Parallel Output https://github.com/FastLED/FastLED/wiki/Parallel-Output
//Pin layouts on OctoWS2811: 2,14,7,8,6,20,21,5


#define NUM_LEDS_PER_STRIP 150
#define NUM_STRIPS 8

CRGB leds[NUM_STRIPS * NUM_LEDS_PER_STRIP];

void setup() {
  FastLED.addLeds<OCTOWS2811>(leds, NUM_LEDS_PER_STRIP);
  //FastLED.setBrightness(32);
}

void loop() {
  /* Demo
  static uint8_t hue = 0;
  for(int i = 0; i < NUM_STRIPS; i++) {
    for(int j = 0; j < NUM_LEDS_PER_STRIP; j++) {
      leds[(i*NUM_LEDS_PER_STRIP) + j] = CHSV((32*i) + hue+j,192,255);
    }
  }

  // Set the first n leds on each strip to show which strip it is
  for(int i = 0; i < NUM_STRIPS; i++) {
    for(int j = 0; j <= i; j++) {
      leds[(i*NUM_LEDS_PER_STRIP) + j] = CRGB::Red;
    }
  }

  hue++;
  */

  FastLED.show();
  FastLED.delay(10);

  //Insert effect functions w/ delays
}
//Effects
void effect1() {}

//Functions
void showStrip() {
 #ifndef ADAFRUIT_NEOPIXEL_H
   FastLED.show();
 #endif
}

void setPixel(int Pixel, byte red, byte green, byte blue) {
 #ifndef ADAFRUIT_NEOPIXEL_H 
   leds[Pixel].r = red;
   leds[Pixel].g = green;
   leds[Pixel].b = blue;
 #endif
}

void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS_PER_STRIP; i++ ) {
    setPixel(i, red, green, blue); 
  }
  showStrip();
}
